package main

import (
	"errors"

	"gitlab.com/gokit/history"
	"gitlab.com/gokit/history/handlers/std"
)


func main() {
	history.SetDefaultHandlers(std.Std)

	// create a history source.
	ctx := history.WithTags("wizzle").WithFields(history.Attrs{
		"rack": 1,
		"deck": 2,
	})

	// log a status for the context to provide
	// progress status.
	ctx.Info("Waiting to count")
	ctx.Yellow("Waiting to count %d", 10)
	ctx.Error(errors.New("bad network"), "Waiting to count")

	ctx.Info("done")

	wackMa()
}

func wackMa() {
	ctx := history.WithFields(history.Attrs{
		"rack": 1,
		"deck": 2,
	})

	ctx.Info("run with the win")
	ctx.Red("bad idea")
	ctx.Yellow("wait now idea")

	ch := make(chan int, 1)
	go func() {
		ctx.Info("In a routine")
		ch <- 1
	}()

	<-ch
}
