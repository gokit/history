package history

import (
	"testing"
)

func BenchmarkHistoryAlloc(b *testing.B) {
	discard := HandlerFunc(func(_ BugLog) error { return nil })

	b.Run("With", func(br *testing.B) {
		br.ReportAllocs()
		br.StopTimer()
		dx := WithHandlers(discard)

		br.StartTimer()
		for i := 0; i < br.N; i++ {
			dx.With("a", "b")
		}
		br.StopTimer()
	})

	b.Run("WithField", func(br *testing.B) {
		br.StopTimer()
		br.ReportAllocs()

		attr := Attrs{"a": "b"}
		logs := WithHandlers(discard).With("a", "b")
		br.StartTimer()
		for i := 0; i < br.N; i++ {
			logs.WithFields(attr)
		}
		br.StopTimer()
	})
}

func BenchmarkIHistory(b *testing.B) {
	discard := HandlerFunc(func(_ BugLog) error { return nil })

	b.Run("NoHandler", func(br *testing.B) {
		br.ReportAllocs()
		br.StopTimer()
		dx := WithTitle("BenchmkarkHistory.NoHandler")

		br.StartTimer()
		for i := 0; i < br.N; i++ {
			dx.IInfo("done")
		}
		br.StopTimer()
	})

	b.Run("OneHandler", func(br *testing.B) {
		br.ReportAllocs()
		br.StopTimer()
		dx := WithHandlers(discard)

		br.StartTimer()
		for i := 0; i < br.N; i++ {
			dx.IInfo("ready")
		}
		br.StopTimer()
	})

	b.Run("PreMade", func(br *testing.B) {
		br.StopTimer()
		br.ReportAllocs()

		logs := WithHandlers(discard).With("a", "b")
		br.StartTimer()
		for i := 0; i < br.N; i++ {
			logs.IInfo("ready")
		}
		br.StopTimer()
	})

	b.Run("WithKV", func(br *testing.B) {
		br.StopTimer()
		br.ReportAllocs()

		logs := WithHandlers(discard)
		br.StartTimer()
		for i := 0; i < br.N; i++ {
			logs.With("a", "b").IInfo("ready")
		}
		br.StopTimer()
	})
}

func BenchmarkHistory(b *testing.B) {
	discard := HandlerFunc(func(_ BugLog) error { return nil })

	b.Run("NoHandler", func(br *testing.B) {
		br.ReportAllocs()
		br.StopTimer()
		dx := WithTitle("BenchmkarkHistory.NoHandler")

		br.StartTimer()
		for i := 0; i < br.N; i++ {
			dx.Info("done")
		}
		br.StopTimer()
	})

	b.Run("OneHandler", func(br *testing.B) {
		br.ReportAllocs()
		br.StopTimer()
		dx := WithHandlers(discard)

		br.StartTimer()
		for i := 0; i < br.N; i++ {
			dx.Info("ready")
		}
		br.StopTimer()
	})

	b.Run("PreMade", func(br *testing.B) {
		br.StopTimer()
		br.ReportAllocs()

		logs := WithHandlers(discard).With("a", "b")
		br.StartTimer()
		for i := 0; i < br.N; i++ {
			logs.Info("ready")
		}
		br.StopTimer()
	})

	b.Run("WithKV", func(br *testing.B) {
		br.StopTimer()
		br.ReportAllocs()

		logs := WithHandlers(discard)
		br.StartTimer()
		for i := 0; i < br.N; i++ {
			logs.With("a", "b").Info("ready")
		}
		br.StopTimer()
	})
}
