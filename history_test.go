package history

import (
	"testing"

	"github.com/influx6/faux/tests"
)

func Test_NoClash(t *testing.T) {
	res := make(chan BugLog, 2)
	log1 := WithHandlers(HandlerFunc(func(b BugLog) error {
		res <- b
		return nil
	})).With("a", "b").Info("ready")

	log1.With("a", "v").Info("ready")

	lg1 := <-res
	lg2 := <-res

	if _, ok := lg1.Fields["a"]; !ok {
		tests.Failed("Should have key in fields")
	}

	if _, ok := lg2.Fields["a"]; !ok {
		tests.Failed("Should have key in fields")
	}

	if lg1.Fields["a"] == lg2.Fields["a"] {
		tests.Info("log1.Field.a: %s", lg1.Fields["a"])
		tests.Info("log2.Field.a: %s", lg2.Fields["a"])
		tests.Failed("Should not have same key with same value")
	}
	tests.Passed("Should not have same key with same value")
}

func Test_WithHandler(t *testing.T) {
	mn := make(chan struct{}, 1)
	src := WithHandlers(HandlerFunc(func(b BugLog) error {
		mn <- struct{}{}
		return nil
	}))

	src.Info("ready")

	select {
	case <-mn:
		tests.Passed("Should have received response from handler")
	default:
		tests.Failed("Should have received response from handler")
	}
}
